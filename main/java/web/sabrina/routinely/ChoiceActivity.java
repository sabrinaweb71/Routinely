package eu.nothingtoshow.routinely;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class ChoiceActivity extends Activity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.choice);
        
        Button morningPerson = (Button)findViewById(R.id.morning);
        Button nightOwl = (Button)findViewById(R.id.night);
        Button unsure = (Button)findViewById(R.id.unsure);
        
        morningPerson.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            SharedPreferences sp = getSharedPreferences("eu.nothingtoshow.routinely", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.putInt("chronotype", 1);
            ed.commit();
            Intent intent = new Intent(ChoiceActivity.this, Main.class);
            startActivity(intent);
          }
        });
        
        nightOwl.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            SharedPreferences sp = getSharedPreferences("eu.nothingtoshow.routinely", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.putInt("chronotype", 2);
            ed.commit();
            Intent intent = new Intent(ChoiceActivity.this, Main.class);
            startActivity(intent);
          }
        });
        
        unsure.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            SharedPreferences sp = getSharedPreferences("eu.nothingtoshow.routinely", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.putInt("chronotype", 3);
            ed.commit();
            Intent intent = new Intent(ChoiceActivity.this, Main.class);
            startActivity(intent);
          }
        });
    }
}
