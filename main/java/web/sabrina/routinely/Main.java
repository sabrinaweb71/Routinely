package eu.nothingtoshow.routinely;

import android.app.*;
import android.content.*;
import android.os.*;
import android.view.*;
import android.widget.*;

public class Main extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.welcome);
        // final Context c = getApplicationContext();
        
        SharedPreferences sp = getSharedPreferences("eu.nothingtoshow.routinely", 0);
        // SharedPreferences.Editor ed = sp.edit();
        // ed.putInt("chronotype", 2);
        // ed.clear();
        // ed.commit();
  
        int chronotype = sp.getInt("chronotype", 0);
        
        String welcome = "Welcome!";
        
        switch (chronotype) {
            case 1:
                welcome = "You are a morning person";
                break;
            case 2:
                welcome = "You are a night owl";
                break;
            case 3:
                welcome = "You are in the middle";
                break;
            default:
                // here I start the second activity
                Intent intent = new Intent(this, ChoiceActivity.class);
                startActivity(intent);
                welcome = "I need to know you better";
                break;
        }
        
        TextView t = findViewById(R.id.welcome);
        t.setText(welcome);
        
        Button reset = (Button)findViewById(R.id.reset);
        reset.setOnClickListener(new View.OnClickListener() {
          public void onClick(View v) {
            SharedPreferences sp = getSharedPreferences("eu.nothingtoshow.routinely", 0);
            SharedPreferences.Editor ed = sp.edit();
            ed.clear();
            ed.commit();
            Intent intent = new Intent(Main.this, ChoiceActivity.class);
            startActivity(intent);
          }
        });
    }
}
