# Routinely

This is meant to be an app to build habits.
When it will be completed, it will remind you the habits that you want to get into.
Features I plan to add:
- the ability to have more than a routine
- a sort of gamification
- the ability to set reminders in generic periods of the day (eg. in the morning, in the evening), rather than exact times
- it will let **you** define when your morning starts
- a way to keep track of how well you are doing in building your habits